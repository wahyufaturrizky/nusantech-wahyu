/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

// React Native Paper Components
import {
  TextInput,
  Checkbox,
  IconButton,
  Button,
  Title,
  Subheading,
} from 'react-native-paper';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

function Home(props) {
  const [state, setState] = useState({
    value_satu: 0,
    value_dua: 0,
    value_tiga: 0,
    checked_satu: false,
    checked_dua: false,
    checked_tiga: false,
    result: 0,
  });

  const {field} = state;

  const onChangeTextField = (e, name) => {
    console.log(name, ':', e);
    setState({
      ...state,
      [name]: e,
    });
  };

  const handleTambah = () => {
    if (
      checked_satu === true &&
      checked_dua === false &&
      checked_tiga === false
    ) {
      alert('Silahkan checklist atau masukkan lebih dari satu field');
    } else if (
      checked_satu === false &&
      checked_dua === true &&
      checked_tiga === false
    ) {
      alert('Silahkan checklist atau masukkan lebih dari satu field');
    } else if (
      checked_satu === false &&
      checked_dua === false &&
      checked_tiga === true
    ) {
      alert('Silahkan checklist atau masukkan lebih dari satu field');
    } else {
      if (
        checked_tiga === true &&
        checked_dua === true &&
        checked_satu === true
      ) {
        console.log('io');
        setState({
          ...state,
          result:
            parseInt(value_tiga) + parseInt(value_dua) + parseInt(value_satu),
        });
      } else if (checked_satu === true && checked_dua === true) {
        console.log('qw');
        setState({
          ...state,
          result: parseInt(value_satu) + parseInt(value_dua),
        });
      } else if (checked_satu === true && checked_tiga === true) {
        console.log('sd');
        setState({
          ...state,
          result: parseInt(value_satu) + parseInt(value_tiga),
        });
      } else if (checked_dua === true && checked_satu === true) {
        console.log('cv');
        setState({
          ...state,
          result: parseInt(value_dua) + parseInt(value_satu),
        });
      } else if (checked_dua === true && checked_tiga === true) {
        console.log('bn');
        setState({
          ...state,
          result: parseInt(value_dua) + parseInt(value_tiga),
        });
      } else if (checked_tiga === true && checked_satu === true) {
        console.log('gh');
        setState({
          ...state,
          result: parseInt(value_tiga) + parseInt(value_satu),
        });
      } else if (checked_tiga === true && checked_dua === true) {
        console.log('ui');
        setState({
          ...state,
          result: parseInt(value_tiga) + parseInt(value_dua),
        });
      }
    }
  };

  const handleKurang = () => {
    if (
      checked_satu === true &&
      checked_dua === false &&
      checked_tiga === false
    ) {
      alert('Silahkan checklist atau masukkan lebih dari satu field');
    } else if (
      checked_satu === false &&
      checked_dua === true &&
      checked_tiga === false
    ) {
      alert('Silahkan checklist atau masukkan lebih dari satu field');
    } else if (
      checked_satu === false &&
      checked_dua === false &&
      checked_tiga === true
    ) {
      alert('Silahkan checklist atau masukkan lebih dari satu field');
    } else {
      if (
        checked_tiga === true &&
        checked_dua === true &&
        checked_satu === true
      ) {
        console.log('io');
        setState({
          ...state,
          result:
            parseInt(value_satu) - parseInt(value_dua) - parseInt(value_tiga),
        });
      } else if (checked_satu === true && checked_dua === true) {
        console.log('qw');
        setState({
          ...state,
          result: parseInt(value_satu) - parseInt(value_dua),
        });
      } else if (checked_satu === true && checked_tiga === true) {
        console.log('sd');
        setState({
          ...state,
          result: parseInt(value_satu) - parseInt(value_tiga),
        });
      } else if (checked_dua === true && checked_satu === true) {
        console.log('cv');
        setState({
          ...state,
          result: parseInt(value_satu) - parseInt(value_dua),
        });
      } else if (checked_dua === true && checked_tiga === true) {
        console.log('bn');
        setState({
          ...state,
          result: parseInt(value_dua) - parseInt(value_tiga),
        });
      } else if (checked_tiga === true && checked_satu === true) {
        console.log('gh');
        setState({
          ...state,
          result: parseInt(value_satu) - parseInt(value_tiga),
        });
      } else if (checked_tiga === true && checked_dua === true) {
        console.log('ui');
        setState({
          ...state,
          result: parseInt(value_dua) - parseInt(value_tiga),
        });
      }
    }
  };

  const handleKali = () => {
    if (
      checked_satu === true &&
      checked_dua === false &&
      checked_tiga === false
    ) {
      alert('Silahkan checklist atau masukkan lebih dari satu field');
    } else if (
      checked_satu === false &&
      checked_dua === true &&
      checked_tiga === false
    ) {
      alert('Silahkan checklist atau masukkan lebih dari satu field');
    } else if (
      checked_satu === false &&
      checked_dua === false &&
      checked_tiga === true
    ) {
      alert('Silahkan checklist atau masukkan lebih dari satu field');
    } else {
      if (
        checked_tiga === true &&
        checked_dua === true &&
        checked_satu === true
      ) {
        console.log('io');
        setState({
          ...state,
          result:
            parseInt(value_satu) * parseInt(value_dua) * parseInt(value_tiga),
        });
      } else if (checked_satu === true && checked_dua === true) {
        console.log('qw');
        setState({
          ...state,
          result: parseInt(value_satu) * parseInt(value_dua),
        });
      } else if (checked_satu === true && checked_tiga === true) {
        console.log('sd');
        setState({
          ...state,
          result: parseInt(value_satu) * parseInt(value_tiga),
        });
      } else if (checked_dua === true && checked_satu === true) {
        console.log('cv');
        setState({
          ...state,
          result: parseInt(value_satu) * parseInt(value_dua),
        });
      } else if (checked_dua === true && checked_tiga === true) {
        console.log('bn');
        setState({
          ...state,
          result: parseInt(value_dua) * parseInt(value_tiga),
        });
      } else if (checked_tiga === true && checked_satu === true) {
        console.log('gh');
        setState({
          ...state,
          result: parseInt(value_satu) * parseInt(value_tiga),
        });
      } else if (checked_tiga === true && checked_dua === true) {
        console.log('ui');
        setState({
          ...state,
          result: parseInt(value_dua) * parseInt(value_tiga),
        });
      }
    }
  };

  const handleBagi = () => {
    if (
      checked_satu === true &&
      checked_dua === false &&
      checked_tiga === false
    ) {
      alert('Silahkan checklist atau masukkan lebih dari satu field');
    } else if (
      checked_satu === false &&
      checked_dua === true &&
      checked_tiga === false
    ) {
      alert('Silahkan checklist atau masukkan lebih dari satu field');
    } else if (
      checked_satu === false &&
      checked_dua === false &&
      checked_tiga === true
    ) {
      alert('Silahkan checklist atau masukkan lebih dari satu field');
    } else {
      if (
        checked_tiga === true &&
        checked_dua === true &&
        checked_satu === true
      ) {
        console.log('io');
        setState({
          ...state,
          result:
            parseInt(value_satu) / parseInt(value_dua) / parseInt(value_tiga),
        });
      } else if (checked_satu === true && checked_dua === true) {
        console.log('qw');
        setState({
          ...state,
          result: parseInt(value_satu) / parseInt(value_dua),
        });
      } else if (checked_satu === true && checked_tiga === true) {
        console.log('sd');
        setState({
          ...state,
          result: parseInt(value_satu) / parseInt(value_tiga),
        });
      } else if (checked_dua === true && checked_satu === true) {
        console.log('cv');
        setState({
          ...state,
          result: parseInt(value_satu) / parseInt(value_dua),
        });
      } else if (checked_dua === true && checked_tiga === true) {
        console.log('bn');
        setState({
          ...state,
          result: parseInt(value_dua) / parseInt(value_tiga),
        });
      } else if (checked_tiga === true && checked_satu === true) {
        console.log('gh');
        setState({
          ...state,
          result: parseInt(value_satu) / parseInt(value_tiga),
        });
      } else if (checked_tiga === true && checked_dua === true) {
        console.log('ui');
        setState({
          ...state,
          result: parseInt(value_dua) / parseInt(value_tiga),
        });
      }
    }
  };

  const {
    value_satu,
    value_dua,
    value_tiga,
    checked_satu,
    checked_dua,
    checked_tiga,
    result,
  } = state;

  return (
    <>
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>
                Calculator for NusanTech by Wahyu Fatur Rizki
              </Text>
              <Text style={styles.sectionDescription}>
                Berikut ini saya kirimkan tes aplikasi kalkulator untuk
                bergabung bersama NusanTech sebagai React Engineer
              </Text>
              <Text style={styles.sectionDescription}>
                <Text style={styles.highlight}>Gitlab :</Text>{' '}
                <Text style={{color: 'blue'}}>bit.ly/gitlabwahyu</Text>
              </Text>
              <Text style={styles.sectionDescription}>
                <Text style={styles.highlight}>Dribbble :</Text>{' '}
                <Text style={{color: 'blue'}}>bit.ly/DribbbleWahyu</Text>
              </Text>
              <View>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    marginTop: 14,
                  }}>
                  <TextInput
                    disabled={checked_satu ? false : true}
                    style={{width: '80%'}}
                    keyboardType="number-pad"
                    label={`Input 1`}
                    value={value_satu}
                    mode="outlined"
                    onChangeText={(value_satu) =>
                      onChangeTextField(value_satu, 'value_satu')
                    }
                  />
                  <Checkbox
                    style={{width: '20%'}}
                    status={checked_satu ? 'checked' : 'unchecked'}
                    onPress={() => {
                      setState({...state, checked_satu: !checked_satu});
                    }}
                  />
                </View>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    marginTop: 14,
                  }}>
                  <TextInput
                    disabled={checked_dua ? false : true}
                    style={{width: '80%'}}
                    keyboardType="number-pad"
                    label={`Input 2`}
                    value={value_dua}
                    mode="outlined"
                    onChangeText={(value_dua) =>
                      onChangeTextField(value_dua, 'value_dua')
                    }
                  />
                  <Checkbox
                    style={{width: '20%'}}
                    status={checked_dua ? 'checked' : 'unchecked'}
                    onPress={() => {
                      setState({...state, checked_dua: !checked_dua});
                    }}
                  />
                </View>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    marginTop: 14,
                  }}>
                  <TextInput
                    disabled={checked_tiga ? false : true}
                    style={{width: '80%'}}
                    keyboardType="number-pad"
                    label={`Input 3`}
                    value={value_tiga}
                    mode="outlined"
                    onChangeText={(value_tiga) =>
                      onChangeTextField(value_tiga, 'value_tiga')
                    }
                  />
                  <Checkbox
                    style={{width: '20%'}}
                    status={checked_tiga ? 'checked' : 'unchecked'}
                    onPress={() => {
                      setState({...state, checked_tiga: !checked_tiga});
                    }}
                  />
                </View>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    marginTop: 14,
                  }}>
                  <Button
                    onPress={handleTambah}
                    style={{width: '50%', marginRight: 4}}
                    mode="contained">
                    Tambah
                  </Button>
                  <Button
                    onPress={handleKurang}
                    style={{width: '50%'}}
                    mode="contained">
                    Kurang
                  </Button>
                </View>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    marginTop: 14,
                  }}>
                  <Button
                    onPress={handleKali}
                    style={{width: '50%', marginRight: 4}}
                    mode="contained">
                    Kali
                  </Button>
                  <Button
                    onPress={handleBagi}
                    style={{width: '50%'}}
                    mode="contained">
                    Bagi
                  </Button>
                </View>

                <Title style={{alignSelf: 'center', marginTop: 12}}>
                  Result = {result}
                </Title>
                <Button
                  onPress={() => {
                    setState({
                      ...state,
                      value_satu: 0,
                      value_dua: 0,
                      value_tiga: 0,
                      checked_satu: false,
                      checked_dua: false,
                      checked_tiga: false,
                      result: 0,
                    });
                  }}
                  style={{
                    width: '50%',
                    alignSelf: 'center',
                    marginTop: 14,
                    marginBottom: 14,
                  }}
                  mode="contained">
                  Reset
                </Button>
                <Subheading style={{textAlign: 'center'}}>
                  App ver 1.0.0
                </Subheading>
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default Home;
